import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;//Packages
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class LandingPage {

	WebDriver pageUnderTest = null;//Comment 
	
	
	public WebDriver getDriverHandle(String stringWebSiteURL)
	{
		String exePath = "D:\\DevOps\\Downloads\\Selenium\\chromedriver_win32\\chromedriver.exe";
		ChromeDriverService.Builder serviceBuilder = new ChromeDriverService.Builder();
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();
		serviceBuilder.withLogFile(new File("D:\\DevOps\\Downloads\\Selenium\\logs\\logFile.txt"));
		driver.get(stringWebSiteURL);
		this.pageUnderTest=driver;
		return driver;//Return Statement
	}
	public static void main(String args[]) throws InterruptedException//Static Method
	{
		LandingPage myPageRef = new LandingPage();//Object Creation
		
		WebDriver webDriver = myPageRef.getDriverHandle("file:///D:/Tasks/Web_Proj/WebContent/login.html");
		
		System.out.println("Title was "+webDriver.getTitle());
		System.out.println("CurrentURL was "+webDriver.getCurrentUrl());
		
		WebElement searchBox= webDriver.findElement(By.name("uname"));
		searchBox.click();
		searchBox.sendKeys("Rajagopal");
		WebElement searchBox1= webDriver.findElement(By.name("psw"));
		searchBox1.click();
		searchBox1.sendKeys("raja");
		webDriver.findElement(By.xpath("/html/body/form/a")).click();;
	}
	
}
